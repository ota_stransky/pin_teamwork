package com.teamwork.stranskyota;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class finnishPin implements Pin {
	
	@Override
	public boolean isPin(String source) {
		if(source==null){return false;}
		if(source.length()!=11){return false;}
		Pattern p = Pattern.compile("^[0123][0-9][01][0-9][0-9][0-9][+|\\-|A][0-9][0-9][0-9][0-9a-zA-Z]$");
		Matcher m = p.matcher(source);
		if (m.find()) {
			if(!isValid(source)){return false;}
            return true;
        } else {
            return false;
        }
	}

	/**
	 * Method examines if given text is valid
	 */
	boolean isValid(String source){
		int y = getRemainder(source);
		char lastCharacter = source.charAt(source.length() - 1);
		if(y < 0 || y > 30){return false;}
		char[] array = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','H','J','K','L','M','N','P','R','S','T','U','V','W','X','Y'};
		if(lastCharacter == array[(int)y]){return true;}
		else{System.out.println("Posledni pismeno: "+lastCharacter+", ale v indexu pole je:"+y);return false;}
	}
	
	int getRemainder(String source){
		double y;
		String x;
		x = source.substring(0,6)+source.substring(7, 10);
		y = Integer.parseInt(x);
		y = y/31;
		y=(y%1)*31;
		y = Math.round(y);
		return (int)y;
	}
}
