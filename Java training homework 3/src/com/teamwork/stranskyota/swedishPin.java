package com.teamwork.stranskyota;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class swedishPin implements Pin {
	
	public String testPIN;			//public variable only for Temporary class 
	@Override
	public boolean isPin(String source) {
		testPIN=source;
		int index100=0;
		
		if (source==null) {
			return false;
		}
		if (source.contains("+")) {
			index100=100;
		}
		source=withoutSigns(source);
		
		if (!controlIsOnlyNumber(source)) {
			return false;
		}
		if (!(source.length()==10 || source.length()==12)) {
			return false;
		}
		if (!swedishValidateDate(source,index100)){
			return false;
		}
		if (source.length()==12){
			source=source.substring(2,12);
		}	
		if (!swedishValidateCheckSum(source)) {
			return false;
		}
	return true;	
	}	
	
	/**
	 * use calendar for Swedish PIN  check date
	 * @param inputPIN, index100
	 * @return true if date from source is correctly
	 */
	private boolean swedishValidateDate(String inputPIN, int index100) {
		Calendar pinCalendar = new GregorianCalendar();
		Calendar todayCalendar = Calendar.getInstance();
		int index=0;
		index=inputPIN.length();
		int yy=Integer.parseInt(inputPIN.substring(0,index-8));
		int mm=Integer.parseInt(inputPIN.substring(index-8,index-6));
		int dd=Integer.parseInt(inputPIN.substring(index-6,index-4));
		//only for stranger in Sweden
		if (dd>60) {
			dd-=60;
		}
		//use calendar for check date from PIN
		try{
		    pinCalendar.setLenient(false);
		    if (inputPIN.length()==10 && yy>(todayCalendar.get(Calendar.YEAR)-2000)){
		    	yy+=1900;
		    }
			else if (inputPIN.length()==10 && yy<(todayCalendar.get(Calendar.YEAR)-1999)) {
				yy+=2000;
			}	
		    pinCalendar.set(yy,mm-1,dd);
		    	if (index100==100) {
		    		pinCalendar.add(Calendar.YEAR,-100);
		    	}
		    //this call exception
		    pinCalendar.getTime();	
			}
		catch (Exception e){		
			return false;
		}
		if (pinCalendar.after(todayCalendar)) {
			return false;
		}
		return true;
	}
	
	
	


	/**
	 * @param source
	 * @return string without unnecessary characters
	 */
	String withoutSigns(String source){
		source=source.replaceAll("[-+/ _().?!;*@\\\"]","");
		return source;
	}
	
	/**
	 * @param source
	 * @return true if source is digits
	 */
	boolean controlIsOnlyNumber (String source){
		for (char sTemp : source.toCharArray()) {
			if (!Character.isDigit(sTemp)) {
				return false;
			} 
		}
		return true;
	}
	
	/**
	 * Validate Swedish PIN 
	 * string -> string array -> integer array and calculation for validation
	 * @param string swedishInput
	 * @return true if special sum of input is divided by 10
	 */
	private boolean swedishValidateCheckSum(String swedishInput) {
		String[] swedishStrings = swedishInput.split("");
		int[] swedishInts = new int[swedishInput.length()];
		int swedishTemp =0, swedishSum=0;
		for (int i = 0; i < swedishInput.length(); i++) {
			swedishInts[i] = Integer.parseInt(swedishStrings[i]);
			if (i%2==0) {
				swedishTemp=swedishInts[i]*2;
					if (swedishTemp>9) {
						swedishTemp-=9;
					}
					
				}
			else {
				swedishTemp=swedishInts[i];
			}
			swedishSum+=swedishTemp;
		}
		if (swedishSum%10==0) { 
			return true;
		} 
		return false;
	}
	
	
}			
