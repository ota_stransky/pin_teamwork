package com.teamwork.stranskyota;

public class czechPin implements Pin {

	int yy, mm, dd;
	@Override
	public boolean isPin(String source) {
		long parseNumber = Long.parseLong(source);
		if  ((source.length() == 10) && parseNumber%11 == 0){
			yy = Integer.parseInt(source.substring(0, 2));
			mm = Integer.parseInt(source.substring(2, 4));
			dd = Integer.parseInt(source.substring(4, 6));
		}
		if (source.length() == 9){
			yy = Integer.parseInt(source.substring(0, 2));
			mm = Integer.parseInt(source.substring(2, 4));
			dd = Integer.parseInt(source.substring(4, 6));
		}
		
		if (isDateOK(dd, mm, yy))
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if date from Czech PIN is valid
	 * @param day
	 * @param month
	 * @param year
	 * @return
	 */
	public boolean isDateOK(int day, int month, int year) {
		if (month > 50)
			month -= 50;
		
		if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
			if (day > 0 && day < 32)		
				return true;
			else
				return false;
		
		if (month == 4 || month == 6 || month == 9 || month == 11)
			if (day > 0 && day < 31)
				return true;
			else
				return false;
		
		if (month == 02 && year % 4 == 0){
			if (day > 0 && day < 30)
				return true;
		}
			else 
				return false;
		
		if (month == 02 && year % 4 !=0){
			if (day > 0 && day < 29)
				return true;
			else
				return false;
			}
		else return false;
	}

}
