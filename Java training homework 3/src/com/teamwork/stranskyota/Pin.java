package com.teamwork.stranskyota;

/**
 * Program for checking PIN validity for individual countries
 * @author TeamWork by Ota Str�nsk�, Vratislav Weinlich, Rostislav Kozusznik
 *
 */
public interface Pin {
	
	/**
	 * Checks if input number is valid as a personal identification number
	 * @param source is input <i>number</i>
	 * @return <i>true/false</i>
	 */
	public boolean isPin(String source);
}
