package com.teamwork.stranskyota;

import java.text.ParseException;
import java.text.SimpleDateFormat;
	/** 
	* modify for use in teamwork - by Vratas
	* @author  Martin Landa
	* @version 1.0
	*/
	public class denmarkPin implements Pin{
		
		public String testPIN;			//public variable only for Temporary class 
		@Override
		public boolean isPin(String source) {
			testPIN=source;
			if(!isEmpty(source)){
				return false;
			}
			source=withoutSigns(source);
			int lengtSSH=source.length();
			if (lengtSSH!=10){
				return false;
			}
			String month=separateSecondTwo(source);
			String year=separateThirdTwo(source);
			String day=separateFirstTwo(source);
			if(!existDate(day, month, year)) {
				return false;
			}
			int [] pole=new int[10];
			pole[0]=Integer.parseInt(source.substring(0,1))*4;
			pole[1]=Integer.parseInt(source.substring(1,2))*3;
			pole[2]=Integer.parseInt(source.substring(2,3))*2;
			pole[3]=Integer.parseInt(source.substring(3,4))*7;
			pole[4]=Integer.parseInt(source.substring(4,5))*6;
			pole[5]=Integer.parseInt(source.substring(5,6))*5;
			pole[6]=Integer.parseInt(source.substring(6,7))*4;
			pole[7]=Integer.parseInt(source.substring(7,8))*3;
			pole[8]=Integer.parseInt(source.substring(8,9))*2;
			pole[9]=Integer.parseInt(source.substring(9,10));
			int vysledek=0;
			for (int i : pole ){
				vysledek+=i;
			}
		
			if (vysledek%11==0){
				return true;
			}
			return false;
		}	
			
		/**
		 * @param source
		 * @return string without unnecessary characters
		 */
		String withoutSigns(String source){
			source=source.replaceAll("[-+/ _().?!;*@\\\"]","");
			return source;
		}
		
		
		/**
		 * Verifies whether source is empty or null
		 * @param sourceSSN		string 
		 * @return			true or false
		 */
		protected  boolean isEmpty(String sourceSSN){
			if (sourceSSN==null){
				return false;
			}
			if (sourceSSN==""){
				return false;
			}
			return true;
		}
		/**
		 * Verifies whether input date can exist
		 * @param day month year		string represent date in form ("dd/MM/yyyy")
		 * @return			true or false
		 */
		protected boolean existDate(String day, String month, String year ) {
			String date=day+"/"+month+"/"+year;
	        try {
	            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	            format.setLenient(false);
	            format.parse(date);
	        } catch (ParseException e) {
	            return false;
	        } catch (IllegalArgumentException e) {
	            return false;
	        }
	        return true;
	    }
		/**
		 * Separates first two characters from source string, is used in method isSSN for separate numebrs from birthday
		 * @param sourceSSN		some string
		 * @return			first two characters 
		 */
		protected String separateFirstTwo(String sourceSSN){
			return sourceSSN.substring(0,2);
		}
		/**
		 * Separates second two characters from source string, is used in method isSSN for separate numebrs from birthday
		 * @param sourceSSN		some string
		 * @return			first two characters 
		 */
		protected String separateSecondTwo(String sourceSSN){
			return sourceSSN.substring(2, 4);
		}
		/**
		 * Separates third two characters from source string, is used in method isSSN for separate numebrs from birthday
		 * @param sourceSSN		some string
		 * @return			first two characters 
		 */
		protected String separateThirdTwo(String sourceSSN){
			return sourceSSN.substring(4, 6);
		}
		
}


	
	
	

