package com.teamwork.stranskyota;

public class norwayPin implements Pin {	

	@Override
	public boolean isPin(String source) {
		int d1, d2, m1, m2, y1, y2, x1, x2, x3, cd1, cd2, controlCd1, controlCd2;
		if (source.length() == 11){
			d1 = Integer.parseInt(source.substring(0, 1)); d2 = Integer.parseInt(source.substring(1, 2));
			m1 = Integer.parseInt(source.substring(2, 3)); m2 = Integer.parseInt(source.substring(3, 4));
			y1 = Integer.parseInt(source.substring(4, 5)); y2 = Integer.parseInt(source.substring(5, 6));
			x1 = Integer.parseInt(source.substring(6, 7)); x2 = Integer.parseInt(source.substring(7, 8));
			x3 = Integer.parseInt(source.substring(8, 9)); 
			cd1 = Integer.parseInt(source.substring(9, 10)); cd2 = Integer.parseInt(source.substring(10, 11));
			
			controlCd1 = 11 - ((3*d1 + 7*d2 + 6*m1 + 1*m2 + 8*y1 + 9*y2 + 4*x1 + 5*x2 + 2*x3)%11);
			if (controlCd1 == 11)	{controlCd1 = 0;}
			
			controlCd2 = 11 - ((5*d1 + 4*d2 + 3*m1 + 2*m2 + 7*y1 + 6*y2 + 5*x1 + 4*x2 + 3*x3 + 2*controlCd1)%11);
			if (controlCd2 == 11)	{controlCd2 = 0;}
			
			if ((controlCd1 == cd1) && (controlCd2 == cd2)){
				return true;
			}
			else return false;
		}
		else return false;
	}
}
